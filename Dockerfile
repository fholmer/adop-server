FROM python:3.10

RUN pip install adop[server]==0.0.5

RUN useradd -ms /bin/bash adop
USER adop

WORKDIR /work
VOLUME /home/adop /work
EXPOSE 8000

CMD [ "python", "-m", "adop", "serve-api" ]
